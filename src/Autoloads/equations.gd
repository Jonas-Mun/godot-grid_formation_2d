extends Node

func total_units(length, unitSize, gap) -> int:
	var distBtxtUnit = unitSize + gap
	var total_units = (length / distBtxtUnit) + 1
	return (length / distBtxtUnit) + 1
	
func total_depth(max_units: int, front_units: int) -> int:
	var full_row = max_units / front_units

	var depths = full_row - 1
	
	# include incomplete row
	if (max_units % front_units > 0):
		depths += 1
	
	return depths
	
func position_on_line(I: Vector2, dist_btxt: float, current_index: int) -> Vector2:
	var distance_away: float = dist_btxt * current_index
	var position = I
	position.x += distance_away
	return position

func position_on_radius(I: Vector2, unit_size: float, gap: float, angle: float, index:int) -> Vector2:
	var dist_btxt = unit_size + gap
	var radius = Vector2(cos(angle), sin(angle)) * index
	radius *= dist_btxt
	
	return I + radius
	
func depth_unit_position(F: Vector2, angle: float, dist_btxt: float) -> Vector2:
	var rad_angle = angle
	# rad_angle = deg2rad(angle) # Weird behaviour if uncommented

	var direction_to_move = Vector2(-sin(rad_angle), cos(rad_angle))
	var distance_away = dist_btxt * direction_to_move
	
	return F + distance_away

func gen_pos_on_radius(I: Vector2, dist_btxt: float, angle: float) -> Vector2:
	var radius_vector = dist_btxt * Vector2(-sin(angle), cos(angle))
	return I + radius_vector

"""
Pascal Equations
"""

func pascal_mid_point(I: Vector2, E: Vector2) -> Vector2:
	var radius = (E-I)
	var half_vector = radius / 2
	var mid_point: Vector2 = I + half_vector
	
	return mid_point
	
func pascal_center_point(mid_point: Vector2, dist_btxt: float, angle: float, depth: int) -> Vector2:
	var center_vector = Vector2(-sin(angle),cos(angle)) * dist_btxt
	var to_depth = center_vector * depth
	var center = mid_point + to_depth
	
	return center
	
func pascal_end_point(center_point: Vector2, dist_btxt: float, remaining_units: int, angle: float) -> Vector2:
	var distance_to_end = (dist_btxt / 2.0) * (remaining_units-1)
	var on_circle = distance_to_end * Vector2(cos(angle),sin(angle))
	var end_point = center_point - on_circle
	
	return end_point
