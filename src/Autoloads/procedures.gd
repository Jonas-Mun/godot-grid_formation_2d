extends Node

func grid_formation_positions(I: Vector2, E: Vector2, unit_size: float, gap: float, max_units: int):
	var radius = (E-I)
	var dist_btxt = unit_size + gap
	var total_front_units = Equations.total_units(radius.length(), unit_size, gap)
	total_front_units = round(total_front_units)
	var rad_angle = Vector2.RIGHT.angle_to(radius)
	var angle = rad_angle
	
	var P = []
	P = positions_on_radius(I, unit_size, gap, angle, total_front_units, max_units)
	P = depth_positions(P, dist_btxt, total_front_units, max_units, angle)
	
	return P

func positions_on_radius(I: Vector2, unit_size: float, gap: float, angle: float, front_units: int, max_units: int) -> Array:
	var P = []
	for current_unit in front_units:
		if current_unit >= max_units:
			return P
		var position = Equations.position_on_radius(I, unit_size, gap, angle,current_unit)
		P.append(position)
	return P
	
func depth_positions(Ps: Array, dist_btxt: float, front_units: int, max_units: int, angle: float) -> Array:
	var current_front_unit = 0
	for depth_unit in range(front_units, max_units):
		var front_position = Ps[current_front_unit]
		var depth_position = Equations.depth_unit_position(front_position, angle, dist_btxt)
		Ps.append(depth_position)
		
		current_front_unit += 1
	return Ps

"""
General procedures
"""
func general_positions(I: Vector2, E: Vector2, unit_size: float, gap: float,  max_units: int):
	var radius = (E-I)
	var total_front_units = Equations.total_units(radius.length(), unit_size, gap)
	total_front_units = round(total_front_units)	# Round to nearest units.
	var total_depth = Equations.total_depth(max_units, total_front_units)

	var dist_btxt = unit_size + gap
	var current_I = I
	var rad_angle = Vector2.RIGHT.angle_to(radius)
	
	var P = []
	
	var current_unit = 0
	# specific depth
	print_debug(total_depth)
	for current_depth in range(0,total_depth+1):
		# Units on line
		for current_position in total_front_units:
			if current_unit >= max_units:
				return P
			var position = Equations.position_on_radius(current_I, unit_size, gap, rad_angle, current_position)
			P.append(position)
			current_unit += 1
		current_I = Equations.gen_pos_on_radius(current_I, dist_btxt, rad_angle)
		
	return P

"""
Pascal Procedures
"""
func pascal_positions(I: Vector2, E: Vector2, depth: int, unit_size: float, gap: float, angle: float, remaining_units: int) -> Array:
	var dist_btxt = unit_size + gap
	var mid_point = Equations.pascal_mid_point(I,E)
	var center_point = Equations.pascal_center_point(mid_point, dist_btxt, angle, depth)
	var end_point = Equations.pascal_end_point(center_point, dist_btxt, remaining_units, angle)
	var P = []
	
	for current_unit in remaining_units:
		var position = Equations.position_on_radius(end_point, unit_size, gap, angle, current_unit)
		P.append(position)
	return P

func general_pascal_positions(I: Vector2, E: Vector2, unit_size: float, gap: float, max_units: int) -> Array:
	var radius = (E-I)
	var angle = Vector2.RIGHT.angle_to(radius)
	var total_front_units = Equations.total_units(radius.length(), unit_size, gap)
	total_front_units = int(round(total_front_units))
	var total_depth = Equations.total_depth(max_units, total_front_units)
	var dist_btxt = unit_size + gap
	var current_I = I
	var remainder_units = max_units % total_front_units
	
	var P = []
	
	for current_depth in range(0,total_depth+1):
		# Check in last row
		if current_depth == total_depth and remainder_units > 0:
			var end_point = Equations.position_on_radius(I, unit_size, gap, angle, min(max_units, total_front_units)-1)
			var pascalPs = pascal_positions(I, end_point, current_depth, unit_size, gap, angle, remainder_units)
			# append pascal positions
			for i in pascalPs.size():
				P.append(pascalPs[i])
			return P
		
		for current_unit in total_front_units:
			var position = Equations.position_on_radius(current_I, unit_size, gap, angle, current_unit)
			P.append(position)
		current_I = Equations.gen_pos_on_radius(current_I, dist_btxt, angle)
	return P
