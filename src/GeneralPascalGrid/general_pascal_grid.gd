extends Node2D


export (String) var mouse_button = "right_click"
export (int) var max_units = 20
export (float) var unit_size = 15
export (float) var gap = 0

var dragging = false

var I: Vector2 = Vector2.ZERO
var C: Vector2 = Vector2.ZERO
var E: Vector2 = Vector2.ZERO

var P: Array = []

func _process(delta):
	var new_dragging =  dragging_input(mouse_button)
	if !dragging && new_dragging:
		dragging = new_dragging
		I = get_global_mouse_position()
	elif dragging && !new_dragging:
		dragging = new_dragging
		E = get_global_mouse_position()
		P = Procedures.general_pascal_positions(I, E, unit_size, gap, max_units)
		update()
		
	if dragging:
		C = get_global_mouse_position()
		P = Procedures.general_pascal_positions(I, C, unit_size, gap, max_units)
		update()
		
		
	
func dragging_input(mouse_b: String) -> bool:
	var dragging = false
	if Input.is_action_pressed(mouse_b):
		dragging = true
	elif Input.is_action_just_released(mouse_b):
		dragging = false
	return dragging

func _draw():
	var color = Color(1,1,1)
	for x in P.size():
		draw_circle(P[x], 5.0, color)
	var mid_point = Equations.pascal_mid_point(I, C)
	draw_circle(mid_point, 6.0, Color(1,0,0))

